﻿using AutoMapper;

namespace EpicUserManager.Client.Common.Mapper.Interface
{
    public interface IMappingConfig
    {
        Profile GetProfile();
    }
}