﻿using Autofac;
using EpicUserManager.Server.Business.Interfaces;
using EpicUserManager.Server.Common.Interfraces;
using EpicUserManager.Server.DataAcces;

namespace EpicUserManager.Server.Business
{
    public static class BusinesContainerRegistration
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<ServerUserHandle>().As<IUserHandler>();
            builder.RegisterType<BusinessMappingConfig>().As<IMappingConfig>();
            DataAccesContainerRegistration.RegisterTypes(builder);
        }
    }
}