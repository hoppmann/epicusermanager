﻿namespace EpicUserManager.Server.Business.Models
{
    public class BusinessUserModel
    {
        public BusinessUserModel(int customerId, string firstName, string lastName)
        {
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
        }

        public int CustomerId { get; }
        public string FirstName { get; }
        public string LastName { get; }
    }
}
