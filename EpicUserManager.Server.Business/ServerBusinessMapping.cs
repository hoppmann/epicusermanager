﻿using AutoMapper;
using EpicUserManager.Server.Business.Models;
using EpicUserManager.Server.DataAcces.Models;

namespace EpicUserManager.Server.Business
{
    public class ServerBusinessMapping : Profile
    {
        public ServerBusinessMapping()
        {
            CreateMap<EntityUserModel, BusinessUserModel>();
            CreateMap<BusinessUserModel, EntityUserModel>();
        }
    }
}
