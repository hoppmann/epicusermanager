﻿using System.Collections.Generic;
using EpicUserManager.Server.Business.Models;

namespace EpicUserManager.Server.Business.Interfaces
{
    public interface IUserHandler
    {
        IList<BusinessUserModel> GetAllUsers();
        void CreateUser(BusinessUserModel user);
        void UpdateUser(BusinessUserModel user);
        void DeleteUser(BusinessUserModel user);
    }
}
