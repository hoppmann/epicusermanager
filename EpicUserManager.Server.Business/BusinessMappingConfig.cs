﻿using AutoMapper;
using EpicUserManager.Server.Common.Interfraces;

namespace EpicUserManager.Server.Business
{
    public class BusinessMappingConfig : IMappingConfig
    {
        public Profile GetProfile()
        {
            return new ServerBusinessMapping();
        }
    }
}