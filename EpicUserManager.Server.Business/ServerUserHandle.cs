﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EpicUserManager.Server.Business.Interfaces;
using EpicUserManager.Server.Business.Models;
using EpicUserManager.Server.DataAcces.Interfaces;
using EpicUserManager.Server.DataAcces.Models;

namespace EpicUserManager.Server.Business
{
    public class ServerUserHandle : IUserHandler
    {
        private readonly IDbCommunication _dbCommunication;


        public ServerUserHandle(IDbCommunication dbCommunication)
        {
            _dbCommunication = dbCommunication;
        }

        public IList<BusinessUserModel> GetAllUsers()
        {
            var users = _dbCommunication.GetAllUsers();
            var result = users.Select(user => Mapper.Map<BusinessUserModel>(user)).ToList();
            return result;

        }

        public void CreateUser(BusinessUserModel user)
        {
            var targetUser = Mapper.Map<EntityUserModel>(user);
            _dbCommunication.CreateUser(targetUser);
        }

        public void UpdateUser(BusinessUserModel user)
        {
            var targetUser = Mapper.Map<EntityUserModel>(user);
            _dbCommunication.UpdateUser(targetUser);
        }

        public void DeleteUser(BusinessUserModel user)
        {
            var targetUser = Mapper.Map<EntityUserModel>(user);
            _dbCommunication.DeleteUser(targetUser);
        }
    }
}
