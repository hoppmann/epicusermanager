﻿using Caliburn.Micro;

namespace EpicUserManager.Client.Presentation.ViewModels
{
    public class UserViewModel: PropertyChangedBase
    {
        public int CustomerId { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
    }
}
