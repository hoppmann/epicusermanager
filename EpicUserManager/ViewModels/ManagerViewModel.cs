﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Caliburn.Micro;
using EpicUserManager.Client.Business.Interfaces;
using EpicUserManager.Client.Business.Models;

namespace EpicUserManager.Client.Presentation.ViewModels
{
    public class ManagerViewModel : PropertyChangedBase
    {
        public IUserHandler UserHandler { get; }

        public ManagerViewModel(IUserHandler userHandler)
        {
            UserHandler = userHandler;
        }

        private void CreateUser()
        {
            var dummy = new UserViewModel
            {
                FirstName = "Max",
                LastName = "Muster"
            };
            SelectedUserViewModel = dummy;
            
        }

        public UserViewModel SelectedUserViewModel { get; set; }
        public UserViewModel DisplayedUserViewModel { get; set; }
        
        public IList<UserViewModel> UserList { get; set; }
        

        public void SaveUser()
        {
            UserHandler.CreateUser(Mapper.Map<BusinessUserModel>(SelectedUserViewModel));
        }

        public void GetAllUsers()
        {
            UserList = UserHandler.GetAllUsers().Select(user =>
                    new UserViewModel() {CustomerId = user.CustomerId, FirstName = user.FirstName, LastName = user.LastName})
                .ToList();            
        }

        public void EditUserInfo()
        {
            UserHandler.UpdateUser(Mapper.Map<BusinessUserModel>(SelectedUserViewModel));
        }

        public void DeleteUser()
        {
            UserHandler.DeleteUser(Mapper.Map<BusinessUserModel>(SelectedUserViewModel));

            GetAllUsers();
        }

    }
}
