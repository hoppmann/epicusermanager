﻿using AutoMapper;
using EpicUserManager.Client.Business.Models;
using EpicUserManager.Client.Presentation.ViewModels;

namespace EpicUserManager.Client.Presentation
{
    public class PresentationBusinessMapping : Profile
    {
        public PresentationBusinessMapping()
        {
            CreateMap<UserViewModel, BusinessUserModel>();
            CreateMap<BusinessUserModel, UserViewModel>();
        }
    }
}