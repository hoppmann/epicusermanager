﻿using System.Windows.Controls;

namespace EpicUserManager.Client.Presentation.Views
{
    /// <summary>
    /// Interaction logic for ManagerView.xaml
    /// </summary>
    public partial class ManagerView : UserControl
    {
        public ManagerView()
        {
            InitializeComponent();
        }
    }
}
