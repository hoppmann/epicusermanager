﻿using System;
using System.Collections.Generic;
using System.Windows;
using Autofac;
using AutoMapper;
using Caliburn.Micro;
using EpicUserManager.Client.Business;
using EpicUserManager.Client.Common.Mapper.Interface;
using EpicUserManager.Client.Presentation.ViewModels;

namespace EpicUserManager.Client.Presentation
{
    class Bootstrapper: BootstrapperBase
    {
        public Bootstrapper()
        {
            Initialize();
        }

        private IContainer Container { get; set; }

        protected override void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<WindowManager>().As<IWindowManager>();
            builder.RegisterType<EventAggregator>().As<IEventAggregator>().SingleInstance();
            builder.RegisterType<ManagerViewModel>();            
            BusinesContainerRegistration.RegisterTypes(builder);
            
            Container = builder.Build();
            InitializeMapper();
        }

        protected override object GetInstance(Type serviceType, string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                if (Container.IsRegistered(serviceType))
                    return Container.Resolve(serviceType);
            }
            else
            {
                if (Container.IsRegisteredWithName(key, serviceType))
                    return Container.ResolveNamed(key, serviceType);
            }
            throw new Exception($"Could not locate any instances of contract {key ?? serviceType.Name}.");
        }

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ManagerViewModel>();
        }

        private void InitializeMapper()
        {
            IEnumerable<IMappingConfig> mappingConfigs = Container.Resolve<IEnumerable<IMappingConfig>>();

            Mapper.Initialize(cfg =>
            {
                foreach (var mappingConfig in mappingConfigs)
                {
                    var profile = mappingConfig.GetProfile();
                    cfg.AddProfile(profile);
                }
            });
        }
    }
}
