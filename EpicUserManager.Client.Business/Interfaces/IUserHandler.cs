﻿using System.Collections.Generic;
using EpicUserManager.Client.Business.Models;

namespace EpicUserManager.Client.Business.Interfaces
{
    public interface IUserHandler
    {
        IList<BusinessUserModel> GetAllUsers();
        void CreateUser(BusinessUserModel user);
        void UpdateUser(BusinessUserModel user);
        void DeleteUser(BusinessUserModel user);
    }
}
