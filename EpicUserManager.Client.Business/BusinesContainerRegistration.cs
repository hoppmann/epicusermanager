﻿using Autofac;
using EpicUserManager.Client.Business.Interfaces;
using EpicUserManager.Client.DataAcces;

namespace EpicUserManager.Client.Business
{
    public static class BusinesContainerRegistration
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
           builder.RegisterType<ClientUserHandler>().As<IUserHandler>();
            DataAccesContainerRegistration.RegisterTypes(builder);
        }
    }
}