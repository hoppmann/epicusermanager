﻿using AutoMapper;
using EpicUserManager.Client.Common.Mapper.Interface;

namespace EpicUserManager.Client.Business
{
    public class BusinessMappingConfig : IMappingConfig
    {
        public Profile GetProfile()
        {
            return new EntityBusinessMapping();
        }
    }
}