﻿using AutoMapper;
using EpicUserManager.Client.Business.Models;
using EpicUserManager.Client.DataAcces.Models;

namespace EpicUserManager.Client.Business
{
    public class EntityBusinessMapping : Profile
    {
        public EntityBusinessMapping()
        {
            CreateMap<EntityUserModel, BusinessUserModel>();
            CreateMap<BusinessUserModel, EntityUserModel>();
        }
    }
}
