﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EpicUserManager.Client.Business.Interfaces;
using EpicUserManager.Client.Business.Models;
using EpicUserManager.Client.DataAcces.Interfaces;
using EpicUserManager.Client.DataAcces.Models;

namespace EpicUserManager.Client.Business
{
    public class ClientUserHandle: IUserHandler
    {
        private readonly IDbCommunication _dbCommunication;

        public ClientUserHandle(IDbCommunication dbCommunication)
        {
            _dbCommunication = dbCommunication;
        }
        public IList<BusinessUserModel> GetAllUsers()
        {
           return (IList<BusinessUserModel>)_dbCommunication.GetAllUsers();
        }

        public void CreateUser(BusinessUserModel user)
        {
             _dbCommunication.CreateUser((EntityUserModel)user);
        }

        public void UpdateUser(BusinessUserModel user)
        {
             _dbCommunication.UpdateUser((EntityUserModel)user);
        }

        public void DeleteUser(BusinessUserModel user)
        {
             _dbCommunication.DeleteUser((EntityUserModel)user);
        }
    }
}
