﻿namespace EpicUserManager.Client.Business.Models
{
    public class BusinessUserModel
    {
        public int CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
