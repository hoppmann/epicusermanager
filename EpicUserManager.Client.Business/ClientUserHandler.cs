﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EpicUserManager.Client.Business.Interfaces;
using EpicUserManager.Client.Business.Models;
using EpicUserManager.Client.DataAcces.Interfaces;
using EpicUserManager.Client.DataAcces.Models;

namespace EpicUserManager.Client.Business
{
    public class ClientUserHandler : IUserHandler
    {
        private readonly IDbCommunication _dbCommunication;
        public ClientUserHandler( IDbCommunication dbCommunication)
        {
            _dbCommunication = dbCommunication;
        }
        public IList<BusinessUserModel> GetAllUsers()
        {
            return _dbCommunication.GetAllUsers().Select(Mapper.Map<BusinessUserModel>).ToList();
        }

        public void CreateUser(BusinessUserModel user)
        {
            _dbCommunication.CreateUser(Mapper.Map<EntityUserModel>(user));
        }

        public void UpdateUser(BusinessUserModel user)
        {
            _dbCommunication.UpdateUser(Mapper.Map<EntityUserModel>(user));
        }

        public void DeleteUser(BusinessUserModel user)
        {
            _dbCommunication.DeleteUser(Mapper.Map<EntityUserModel>(user));
        }
    }
}
