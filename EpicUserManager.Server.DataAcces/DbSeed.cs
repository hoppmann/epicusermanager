﻿using System.Collections.Generic;
using EpicUserManager.Server.DataAcces.Interfaces;
using EpicUserManager.Server.DataAcces.Models;

namespace EpicUserManager.Server.DataAcces
{
    public class DbSeed : IDbSeed
    {
        private List<EntityUserModel> GetListOfUsers()
        {
            return new List<EntityUserModel>()
            {
                new EntityUserModel(){FirstName = "Vorname1",LastName = "Nachname1"},
                new EntityUserModel(){FirstName = "Vorname2",LastName = "Nachname2"},
                new EntityUserModel(){FirstName = "Vorname3",LastName = "Nachname3"},
                new EntityUserModel(){FirstName = "Vorname4",LastName = "Nachname4"}
            };
        }

        public void SeedSomeUsers(MyNHibernateSession session)
        {
            using (var trans = session.BeginTransaction())
            {
                GetListOfUsers().ForEach(session.Save);                
                trans.Commit();
            }
        }
    }
}
