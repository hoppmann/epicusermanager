﻿using System.Collections.Generic;
using EpicUserManager.Server.DataAcces.Models;

namespace EpicUserManager.Server.DataAcces.Interfaces
{
    public interface IDbCommunication
    {
        IList<EntityUserModel> GetAllUsers();
        void CreateUser(EntityUserModel user);
        void UpdateUser(EntityUserModel user);
        void DeleteUser(EntityUserModel user);
    }
}
