﻿namespace EpicUserManager.Server.DataAcces.Interfaces
{
    public interface IDbSeed
    {
        void SeedSomeUsers(MyNHibernateSession session);
    }
}