﻿using NHibernate;

namespace EpicUserManager.Server.DataAcces.Interfaces
{
    public interface IFluentNHibernateHelper
    {
        MyNHibernateSession NHibernateSession { get; }
        IStatelessSession StatelessSession { get; }
        void InitializeDatabase();
        void Clear();
        void SetSession(MyNHibernateSession nHibernateSession);
        void DisposeStaticConfiguration();
        void ResetDataSourceInitialized();
        void OnSessionCreated(MyNHibernateSession nHibernateSession);
        void OnSessionDisposed(MyNHibernateSession nHibernateSession);        
        MyNHibernateSession CreateSession();
    }
}