﻿using Autofac;
using EpicUserManager.Server.DataAcces.Interfaces;

namespace EpicUserManager.Server.DataAcces
{
    public static class DataAccesContainerRegistration
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<FluentNHibernateHelper>().As<IFluentNHibernateHelper>();
            builder.RegisterType<DbCommunication>().As<IDbCommunication>();
            builder.RegisterType<DbSeed>().As<IDbSeed>();
        }
    }
}