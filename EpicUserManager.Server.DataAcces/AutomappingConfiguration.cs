﻿using System;
using EpicUserManager.Server.DataAcces.Models;
using FluentNHibernate.Automapping;

namespace EpicUserManager.Server.DataAcces
{
    public class AutomappingConfiguration : DefaultAutomappingConfiguration
    {
        public override bool ShouldMap(Type type)
        {
            return type == typeof(EntityUserModel);
        }
    }
}