﻿using System;
using System.Linq;
using EpicUserManager.Server.DataAcces.Interfaces;
using NHibernate;

namespace EpicUserManager.Server.DataAcces
{
    public class MyNHibernateSession : IDisposable
    {
        private readonly IFluentNHibernateHelper _helper;        
        private ISession _internalSession;        

        public MyNHibernateSession(IFluentNHibernateHelper helper, ISession session)
        {
            _helper = helper;
            InternalSession = session;
        }

        public ISession InternalSession
        {
            get
            {
                if (_internalSession?.IsOpen == false)
                    _internalSession = _helper.CreateSession().InternalSession;
                return _internalSession;
            }
            private set => _internalSession = value;
        }

        public void Dispose()
        {
            Disposer.DisposeAndClear(ref _internalSession);            
        }

        public void Clear()
        {
            InternalSession.Clear();
        }

        public ITransaction BeginTransaction()
        {
            return InternalSession.BeginTransaction();
        }

        public IQueryable<T> Query<T>()
        {
            return InternalSession.Query<T>();
        }

        public void Update(object value)
        {
            InternalSession.Update(value);
        }

        public void Save(object value)
        {
            InternalSession.Save(value);
        }

        public void Delete(object value)
        {
            InternalSession.Delete(value);
        }

        public void SaveOrUpdate(object value)
        {
            InternalSession.SaveOrUpdate(value);
        }
    }
}