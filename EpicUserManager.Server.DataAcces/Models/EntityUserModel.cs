﻿namespace EpicUserManager.Server.DataAcces.Models
{
    public class EntityUserModel
    {
        public virtual int CustomerId { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
    }
}
