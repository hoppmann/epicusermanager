﻿using EpicUserManager.Server.DataAcces.Models;
using FluentNHibernate.Automapping;
using FluentNHibernate.Automapping.Alterations;

namespace EpicUserManager.Server.DataAcces
{
    public class EntityUserMappingOverride : IAutoMappingOverride<EntityUserModel> 
    {
        public void Override(AutoMapping<EntityUserModel> mapping)
        {            
            mapping.References(x => x.FirstName).Not.Nullable();
            mapping.References(x => x.LastName).Not.Nullable();
            mapping.Id(x => x.CustomerId).GeneratedBy.Assigned();
        }
    }
}