﻿using System.Collections.Generic;
using System.Linq;
using EpicUserManager.Server.DataAcces.Interfaces;
using EpicUserManager.Server.DataAcces.Models;

namespace EpicUserManager.Server.DataAcces
{
    public class DbCommunication : IDbCommunication
    {
        private readonly IFluentNHibernateHelper _fluentNHibernateHelper;

        public DbCommunication(IFluentNHibernateHelper fluentNHibernateHelper)
        {
            _fluentNHibernateHelper = fluentNHibernateHelper;
        }

        public IList<EntityUserModel> GetAllUsers()
        {
            using (var session = _fluentNHibernateHelper.CreateSession())
            {
                return session.Query<EntityUserModel>().Select(x => x).ToList();
            }
        }

        public void CreateUser(EntityUserModel user)
        {
            using (var session = _fluentNHibernateHelper.CreateSession())
            {
                using (var trans = session.BeginTransaction())
                {                                        
                    session.Save(user);
                    trans.Commit();
                }
            }
        }

        public void UpdateUser(EntityUserModel user)
        {
            using (var session = _fluentNHibernateHelper.CreateSession())
            {
                using (var trans = session.BeginTransaction())
                {                    
                    var selecteduser = session.Query<EntityUserModel>().Single<EntityUserModel>(x => x.CustomerId == user.CustomerId);
                    var updateduser = new EntityUserModel(){FirstName = user.FirstName, LastName = user.LastName};
                    session.Update(updateduser);    
                    trans.Commit();
                }
                
            }
        }

        public void DeleteUser(EntityUserModel user)
        {
            using (var session = _fluentNHibernateHelper.CreateSession())
            {
                using (var trans = session.BeginTransaction())
                {
                    session.Delete(user);
                    trans.Commit();
                }
            }
        }
    }
}
