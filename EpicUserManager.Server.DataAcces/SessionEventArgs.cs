﻿namespace EpicUserManager.Server.DataAcces
{
    public class SessionEventArgs
    {
        private MyNHibernateSession _nHibernateSession;

        public SessionEventArgs(MyNHibernateSession nHibernateSession)
        {
            this._nHibernateSession = nHibernateSession;
        }
    }
}