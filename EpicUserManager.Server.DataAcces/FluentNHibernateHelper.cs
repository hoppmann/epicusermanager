﻿using System;
using EpicUserManager.Server.DataAcces.Interfaces;
using EpicUserManager.Server.DataAcces.Models;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace EpicUserManager.Server.DataAcces
{
    public class EntityUserMap : FluentNHibernate.Mapping.ClassMap<EntityUserModel>
    {
        public EntityUserMap()
        {
            Id(r => r.CustomerId);
            Map(r => r.FirstName);
            Map(r => r.LastName);            
        }
    }

    public class FluentNHibernateHelper : IFluentNHibernateHelper
    {
        private readonly IDbSeed _seedhelper;
        private static readonly object DatabaseInitializationLock = new object();
        private static global::NHibernate.Cfg.Configuration _configuration;
        private static ISessionFactory _sessionFactory;
        private static bool _databaseInitialized;

        private static readonly string _servername = @"(localdb)\MSSQLLocalDB"; 
        private static readonly string _username = "";
        private static readonly string _passwort = "";
        private static readonly string _databasename = "EpicUserManager";

        //private static readonly string ConnectionString = $"Server=tcp:{ _servername },1433;Initial Catalog={ _databasename };Persist Security Info=False;User ID={_username};Password={_passwort};MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";        
        private static readonly string ConnectionString = @"Server=(localdb)\\mssqllocaldb;Database=EpicUserManager;Trusted_Connection=True;MultipleActiveResultSets=true";

        private MyNHibernateSession _session;
        private IStatelessSession _statelessSession;

        public static event EventHandler<SessionEventArgs> SessionCreated;
        public static event EventHandler<SessionEventArgs> SessionDisposed;
       
        public MyNHibernateSession NHibernateSession => _session ?? (_session = CreateSession());

        public FluentNHibernateHelper(IDbSeed seedhelper)
        {
            _seedhelper = seedhelper;
        }

        public IStatelessSession StatelessSession
        {
            get
            {
                Initialize();
                if (_statelessSession != null)
                    return _statelessSession;
                _statelessSession = SessionFactory.OpenStatelessSession();
                return _statelessSession;
            }
        }

        private ISessionFactory SessionFactory
        {
            get
            {
                lock (DatabaseInitializationLock)
                    return _sessionFactory ?? (_sessionFactory = GetConfig().BuildSessionFactory());
            }
        }

        public void InitializeDatabase()
        {
            Initialize();
        }

        public void Clear()
        {
            _session?.Clear();
        }

        public void SetSession(MyNHibernateSession nHibernateSession)
        {
            _session = nHibernateSession;
        }

        public void DisposeStaticConfiguration()
        {
            lock (DatabaseInitializationLock)
                Disposer.DisposeAndClear(ref _sessionFactory);
        }

        public void ResetDataSourceInitialized()
        {
            lock (DatabaseInitializationLock)
                _databaseInitialized = false;
        }

        public void OnSessionCreated(MyNHibernateSession nHibernateSession)
        {
            SessionCreated?.Invoke(this, new SessionEventArgs(nHibernateSession));
        }

        public void OnSessionDisposed(MyNHibernateSession nHibernateSession)
        {
            SessionDisposed?.Invoke(this, new SessionEventArgs(nHibernateSession));
        }    

        MyNHibernateSession IFluentNHibernateHelper.CreateSession()
        {
            if (_session?.InternalSession == null || _session.InternalSession.IsOpen == false) _session = CreateSession();
            return _session;
        }

        protected internal string GetTableName<T>()
        {
            return _configuration.GetClassMapping(typeof(T)).RootTable.Name;
        }

        protected void DropDatabase()
        {            
            _databaseInitialized = false;
        }

        private MyNHibernateSession CreateSession()
        {
            Initialize();
            return new MyNHibernateSession(this, SessionFactory.OpenSession());
        }
        
        private void Initialize()
        {
            lock (DatabaseInitializationLock)
            {
                if (_databaseInitialized)
                    return;
                _databaseInitialized = true;

                _configuration = Fluently.Configure()
                    .Database(MsSqlConfiguration.MsSql2012.ConnectionString(
                        "Server=(localdb)\\mssqllocaldb;Database=EpicUserManager;Trusted_Connection=True;MultipleActiveResultSets=true"))
                    .Mappings(m =>
                        m.FluentMappings
                            .AddFromAssemblyOf<EntityUserModel>())
                    ////.ExposeConfiguration(cfg => new SchemaUpdate(cfg).Execute(true, true))
                    .BuildConfiguration();

                ////var automappingConfiguration = new AutomappingConfiguration();

                ////var persistenceModel = AutoMap
                ////    .AssemblyOf<EntityUserModel>(automappingConfiguration)
                ////    .UseOverridesFromAssemblyOf<AutomappingConfiguration>()
                ////    .UseOverridesFromAssemblyOf<EntityUserMappingOverride>();

                ////////MsSqlConfiguration.MsSql2012.ConnectionString(ConnectionString)

                ////_configuration = Fluently.Configure()
                ////    .Database(MsSqlConfiguration.MsSql2012.ConnectionString("Server=(localdb)\\mssqllocaldb;Database=EpicUserManager;Trusted_Connection=True;MultipleActiveResultSets=true"))
                ////    .Mappings(
                ////        m =>
                ////        {
                ////            m.AutoMappings.Add(persistenceModel);
                ////        })
                ////    .BuildConfiguration();

                SchemaValidator(_configuration);
            }
        }

        private Configuration GetConfig()
        {
            lock (DatabaseInitializationLock)
            {
                if (_configuration != null)
                    return _configuration;
                else
                    Initialize();
                return _configuration;
            }
        }

        private void SchemaValidator(Configuration config)
        {
            var validator = new SchemaValidator(config);
            try
            {
                validator.Validate();
            }
            catch (HibernateException)
            {
                // not valid, try to update
                try
                {
                    var update = new SchemaUpdate(config);
                    update.Execute(false, true);
                    
                    using (var session = CreateSession())
                    {
                        _seedhelper.SeedSomeUsers(session);
                        // no database need to seed it
                    }                    
                }
                catch (HibernateException)
                {
                    throw;
                }
            }
        }
    }    
}