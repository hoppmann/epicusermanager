﻿using System;

namespace EpicUserManager.Server.DataAcces
{
    public static class Disposer
    {
        public static void DisposeAndClear<T>(ref T disposable)
            where T : IDisposable
        {
            if (disposable == null)
                return;

            disposable.Dispose();
            disposable = default(T);
        }
    }
}