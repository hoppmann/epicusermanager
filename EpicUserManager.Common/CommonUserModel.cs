﻿using System.ComponentModel.DataAnnotations;
namespace EpicUserManager.Common
{
    public class CommonUserModel
    {
        [Required]
        public int CustomerId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
    }
}


