﻿using AutoMapper;
using EpicUserManager.Client.DataAcces.Models;
using EpicUserManager.Common;

namespace EpicUserManager.Client.DataAcces
{
    public class EntityCommonMapping : Profile
    {
        public EntityCommonMapping()
        {
            CreateMap<EntityUserModel, CommonUserModel>();
            CreateMap<CommonUserModel, EntityUserModel>();
        }
    }
}