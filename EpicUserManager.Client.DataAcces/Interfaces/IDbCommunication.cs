﻿using System.Collections.Generic;
using EpicUserManager.Client.DataAcces.Models;

namespace EpicUserManager.Client.DataAcces.Interfaces
{
    public interface IDbCommunication
    {
        IList<EntityUserModel> GetAllUsers();
        void CreateUser(EntityUserModel user);
        void UpdateUser(EntityUserModel user);
        void DeleteUser(EntityUserModel user);
    }
}
