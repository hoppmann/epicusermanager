﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using EpicUserManager.Client.DataAcces.Interfaces;
using EpicUserManager.Client.DataAcces.Models;

namespace EpicUserManager.Client.DataAcces
{
    public class ApiCommunication : IDbCommunication
    {
        public IList<EntityUserModel> GetAllUsers()
        {
            using (var w = new WebClient())
            {
                var jsonData = w.DownloadString("http://localhost:64652/api/values/currencies");
                if (!string.IsNullOrEmpty(jsonData))
                    return JsonConvert.DeserializeObject<List<CommonUserModel>>(jsonData);
                return null;
                ////  return CurrencyRate.EmptyContainer();
            }
        }

        public void CreateUser(EntityUserModel user)
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(EntityUserModel user)
        {
            throw new NotImplementedException();
        }

        public void DeleteUser(EntityUserModel user)
        {
            throw new NotImplementedException();
        }
    }
}
