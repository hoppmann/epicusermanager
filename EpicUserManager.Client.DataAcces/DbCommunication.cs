﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using AutoMapper;
using EpicUserManager.Client.DataAcces.Interfaces;
using EpicUserManager.Client.DataAcces.Models;
using EpicUserManager.Common;
using Newtonsoft.Json;

namespace EpicUserManager.Client.DataAcces
{
    public class DbCommunication : IDbCommunication
    {
        private string _baseUri = "http://localhost:52664/api/users/";

        public IList<EntityUserModel> GetAllUsers()
        {
            var apiUri = CreateApiUri(ApiAction.AllUsers);
            return
                RequestFromApi<IList<CommonUserModel>>(apiUri)
                    .Select(Mapper.Map<EntityUserModel>)
                    .ToList();
        }

        public void CreateUser(EntityUserModel user)
        {
            PostToApi(CreateHttpRequestMessage(ApiAction.CreateUser, user));
        }

        public void UpdateUser(EntityUserModel user)
        {
            PostToApi(CreateHttpRequestMessage(ApiAction.UpdateUser, user));
        }

        public void DeleteUser(EntityUserModel user)
        {
            PostToApi(CreateHttpRequestMessage(ApiAction.DeleteUser, user));
        }

        private void PostToApi(HttpRequestMessage messege)
        {
            using (var client = new HttpClient())
            {
                var result = client.SendAsync(messege).Result;
                //var status = result.StatusCode;
                //TODO: StatusCodeHandling
            }
        }

        private TResult RequestFromApi<TResult>(string apiAdress)
        {
            using (var client = new HttpClient())
            {
                var result = client.GetAsync(apiAdress).Result;

                var serialData = result.Content.ReadAsStringAsync().Result;
                return JsonConvert.DeserializeObject<TResult>(serialData);
            }
        }

        private string CreateApiUri(ApiAction action, EntityUserModel user = null)
        {
           
            var actionString = action.ToString().ToLower();

            if (user == null)
                return _baseUri + $"{actionString}";

            var dataUser = Mapper.Map<CommonUserModel>(user);

            var paramstring = $"{nameof(dataUser.CustomerId)}={dataUser.CustomerId}&" +
                              $"{nameof(dataUser.FirstName)}={dataUser.FirstName}&" +
                              $"{nameof(dataUser.LastName)}={dataUser.LastName}";

            return _baseUri + $"{actionString}/?{paramstring}";
        }

        private HttpRequestMessage CreateHttpRequestMessage(ApiAction action, EntityUserModel user)
        {

            if (user == null)
                throw new ArgumentNullException(
                    $"{nameof(CreateHttpRequestMessage)} at {nameof(EntityUserModel)}: parameter was null");

            return new HttpRequestMessage(GetHttpMethod(action), CreateApiUri(action, user));
            //return new HttpRequestMessage(GetHttpMethod(action), CreateApiUri(action));
        }

        private HttpMethod GetHttpMethod(ApiAction action)
        {
            switch (action)
            {
                case ApiAction.AllUsers:
                    return HttpMethod.Get;
                case ApiAction.CreateUser:
                    return HttpMethod.Put;
                case ApiAction.UpdateUser:
                    return HttpMethod.Post;
                case ApiAction.DeleteUser:
                    return HttpMethod.Delete;
                default:
                    throw new ArgumentOutOfRangeException(nameof(action), action, null);
            }
        }

        private enum ApiAction
        {
            AllUsers,
            CreateUser,
            UpdateUser,
            DeleteUser
        }
    }
}