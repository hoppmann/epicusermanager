﻿namespace EpicUserManager.Client.DataAcces.Models
{
    public class EntityUserModel
    {
        public EntityUserModel(int customerId, string firstName, string lastName)
        {
            CustomerId = customerId;
            FirstName = firstName;
            LastName = lastName;
        }

        public int CustomerId { get; }
        public string FirstName { get; }
        public string LastName { get; }
    }
}
