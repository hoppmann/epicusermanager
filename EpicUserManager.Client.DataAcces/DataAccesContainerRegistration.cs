﻿using Autofac;
using EpicUserManager.Client.DataAcces.Interfaces;

namespace EpicUserManager.Client.DataAcces
{
    public static class DataAccesContainerRegistration
    {
        public static void RegisterTypes(ContainerBuilder builder)
        {
            builder.RegisterType<DbCommunication>().As<IDbCommunication>();
           
        }
    }
}