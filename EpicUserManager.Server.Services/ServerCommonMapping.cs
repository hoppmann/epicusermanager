﻿using AutoMapper;
using EpicUserManager.Common;
using EpicUserManager.Server.Business.Models;

namespace EpicUserManager.Server.Services
{
    public class ServerCommonMapping : Profile
    {
        public ServerCommonMapping()
        {
            CreateMap<BusinessUserModel, CommonUserModel>();
            CreateMap<CommonUserModel, BusinessUserModel>();
        }
    }
}