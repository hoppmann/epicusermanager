﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using EpicUserManager.Server.Business;
using EpicUserManager.Server.Common.Interfraces;

namespace EpicUserManager.Server.Services
{
    public class IocContainer
    {
        public void Initialze()
        {
            Configure();

            var config = GlobalConfiguration.Configuration;

            config.DependencyResolver = new AutofacWebApiDependencyResolver(Container);
        }

        private IContainer Container { get; set; }

        private void Configure()
        {
            var builder = new ContainerBuilder();
        
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<ServicesMappingConfig>().As<IMappingConfig>();
            
            BusinesContainerRegistration.RegisterTypes(builder);

            Container = builder.Build();

            InitializeMapper();
        }

        private void InitializeMapper()
        {
            IEnumerable<IMappingConfig> mappingConfigs = Container.Resolve<IEnumerable<IMappingConfig>>();
            
            Mapper.Initialize(cfg =>
            {
                foreach (var mappingConfig in mappingConfigs)
                {
                    var profile = mappingConfig.GetProfile();
                    cfg.AddProfile(profile);
                }
            });
        }

        protected object GetInstance(Type serviceType, string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                if (Container.IsRegistered(serviceType))
                    return Container.Resolve(serviceType);
            }
            else
            {
                if (Container.IsRegisteredWithName(key, serviceType))
                    return Container.ResolveNamed(key, serviceType);
            }
            throw new Exception($"Could not locate any instances of contract {key ?? serviceType.Name}.");
        }
    }
}
