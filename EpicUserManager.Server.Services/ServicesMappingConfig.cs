using AutoMapper;
using EpicUserManager.Server.Common.Interfraces;

namespace EpicUserManager.Server.Services
{
    public class ServicesMappingConfig : IMappingConfig
    {
        public Profile GetProfile()
        {
            return new ServerCommonMapping();
        }
    }
}