﻿using System.Collections.Generic;
using System.Web.Http;
using AutoMapper;
using EpicUserManager.Common;
using EpicUserManager.Server.Business.Interfaces;
using EpicUserManager.Server.Business.Models;

namespace EpicUserManager.Server.Services.Controllers
{
    public class UsersController : ApiController
    {
        public IUserHandler UserHandler { get; }

        public UsersController(IUserHandler userHandler)
        {
            UserHandler = userHandler;
        }

        [HttpGet]
        public IEnumerable<CommonUserModel> AllUsers()
        {
            var users = UserHandler.GetAllUsers();
            
            foreach (var user in users)
            {
                yield return Mapper.Map<CommonUserModel>(user);
            }
        }

        [HttpPost]
        public void CreateUser([FromUri]CommonUserModel user)
        {
            UserHandler.CreateUser(Mapper.Map<BusinessUserModel>(user));
        }
        
        [HttpPost]
        public void UpdateUser([FromUri]CommonUserModel user)
        {
            UserHandler.UpdateUser(Mapper.Map<BusinessUserModel>(user));
        }
        
        [HttpDelete]
        public void DeleteUser([FromUri]CommonUserModel user)
        {
            UserHandler.DeleteUser(Mapper.Map<BusinessUserModel>(user));
        }
    }
}