﻿using AutoMapper;

namespace EpicUserManager.Server.Common.Interfraces
{
    public interface IMappingConfig
    {
        Profile GetProfile();
    }
}